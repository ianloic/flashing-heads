CC=msp430-gcc
CXX=msp430-g++
LD=msp430-ld
#CFLAGS=-mmcu=msp430x2211
CFLAGS=-O2


all: run

run: prog
	mspdebug rf2500 run

prog: blink.elf
	mspdebug rf2500 "prog blink.elf"

blink.elf: blink.c
	$(CC) $(CFLAGS) -o blink.elf blink.c

clean:
	rm -f blink.elf
