#include  <msp430g2553.h>
#include <stdlib.h>

typedef int (*durationFunc)();

struct LED {
  int pin;
  durationFunc on;
  durationFunc off;
  int state;
  int counter;
};

int strobeOn() { return 1; }
int strobeOff() { return 16 + (rand() % 16); }

int flickerOn() { return 16 + (rand() % 16); }
int flickerOff() { return 1; }

struct LED LEDs[] = {
  { BIT0, strobeOn, strobeOff },
  { BIT6, flickerOn, flickerOff },
  { -1, NULL, NULL},
};

/* run a single iteration */
void process(void) {
  int i;
  for (i=0; LEDs[i].pin >= 0; i++) {
    if (LEDs[i].counter <= 0) {
      if (LEDs[i].state) {
        /* turn it off */
        P1OUT &= ~LEDs[i].pin;
        LEDs[i].counter = (LEDs[i].off)();
      } else {
        /* turn it on */
        P1OUT |= LEDs[i].pin;
        LEDs[i].counter = (LEDs[i].on)();
      }
      LEDs[i].state = !LEDs[i].state;
    } else {
      LEDs[i].counter--;
    }
  }
}

void main(void) {
  /* Set watchdog timer interval to 32ms */
  WDTCTL = WDT_MDLY_32;

  /* "Interrupt enable 1" for the Watchdog Timer interrupt */
  IE1 |= WDTIE;

  /* Work out which pins we're using, set the pins to output and turn them off */
  int pins = 0;
  int i;
  for (i=0; LEDs[i].pin >= 0; i++) {
    P1DIR |= LEDs[i].pin;
    P1OUT &= ~LEDs[i].pin;
  }

  process();

  /* Go into low power mode 0, general interrupts enabled */
  __bis_SR_register( LPM0_bits + GIE );

  /* Do nothing...forever */
  for( ; ; ) { }
}

/* Watchdog Timer interrupt service routine.  The function prototype
 * tells the compiler that this will service the Watchdog Timer, and
 * then the function follows.
 */
unsigned int wdtCounter = 0;
void watchdog_timer(void) __attribute__((interrupt(WDT_VECTOR)));
void watchdog_timer(void)
{
  process();
  /* Go back to low power mode 0 until the next interrupt */
  __bis_SR_register_on_exit( LPM0_bits );
}

